#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Top Block
# Generated: Mon Apr 29 15:37:18 2013
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio import window
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import forms
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import threading
import time
import wx

class top_block(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Top Block")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.probe_var = probe_var = 0
		self.samp_rate = samp_rate = 1000000
		self.probed_value = probed_value = probe_var

		##################################################
		# Blocks
		##################################################
		self.probe_vsrc = gr.probe_signal_f()
		def _probe_var_probe():
			while True:
				val = self.probe_vsrc.level()
				try: self.set_probe_var(val)
				except AttributeError, e: pass
				time.sleep(1.0/(1000))
		_probe_var_thread = threading.Thread(target=_probe_var_probe)
		_probe_var_thread.daemon = True
		_probe_var_thread.start()
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "tab1")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "tab2")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "tab3")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_1 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=2,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_1.win)
		self.wxgui_fftsink2_0 = fftsink2.fft_sink_c(
			self.notebook_0.GetPage(1).GetWin(),
			baseband_freq=0,
			y_per_div=10,
			y_divs=10,
			ref_level=0,
			ref_scale=2.0,
			sample_rate=samp_rate,
			fft_size=1024,
			fft_rate=15,
			average=False,
			avg_alpha=None,
			title="FFT Plot",
			peak_hold=False,
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_fftsink2_0.win)
		self.uhd_usrp_source_0 = uhd.usrp_source(
			device_addr="",
			stream_args=uhd.stream_args(
				cpu_format="fc32",
				channels=range(1),
			),
		)
		self.uhd_usrp_source_0.set_samp_rate(samp_rate)
		self.uhd_usrp_source_0.set_center_freq(500000000+probe_var*100000000, 0)
		self.uhd_usrp_source_0.set_gain(27, 0)
		self.uhd_usrp_sink_0 = uhd.usrp_sink(
			device_addr="",
			stream_args=uhd.stream_args(
				cpu_format="fc32",
				channels=range(1),
			),
		)
		self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
		self.uhd_usrp_sink_0.set_center_freq(500000000+probe_var*100000000, 0)
		self.uhd_usrp_sink_0.set_gain(27, 0)
		self._probed_value_static_text = forms.static_text(
			parent=self.GetWin(),
			value=self.probed_value,
			callback=self.set_probed_value,
			label='probed_value',
			converter=forms.float_converter(),
		)
		self.Add(self._probed_value_static_text)
		self.low_pass_filter_0 = gr.fir_filter_ccf(1, firdes.low_pass(
			1, samp_rate, 10000, 1000, firdes.WIN_HAMMING, 6.76))
		self.gr_vco_f_0 = gr.vco_f(samp_rate, 1*(2*3.14)*1000, 1)
		self.gr_throttle_0_0 = gr.throttle(gr.sizeof_gr_complex*1, samp_rate)
		self.gr_throttle_0 = gr.throttle(gr.sizeof_gr_complex*1, samp_rate)
		self.gr_threshold_ff_0_2 = gr.threshold_ff(3, 4, 0)
		self.gr_threshold_ff_0_1 = gr.threshold_ff(2, 3, 0)
		self.gr_threshold_ff_0_0 = gr.threshold_ff(1, 2, 0)
		self.gr_threshold_ff_0 = gr.threshold_ff(1, 1, 0)
		self.gr_sub_xx_0 = gr.sub_ff(1)
		self.gr_sig_source_x_0 = gr.sig_source_f(samp_rate, gr.GR_SAW_WAVE, 1, 5, 0)
		self.gr_multiply_xx_0_0 = gr.multiply_vcc(1)
		self.gr_multiply_xx_0 = gr.multiply_vcc(1)
		self.gr_hilbert_fc_0 = gr.hilbert_fc(64)
		self.gr_add_xx_0 = gr.add_vff(1)
		self.const_source_x_0 = gr.sig_source_c(0, gr.GR_CONST_WAVE, 0, 0, 4)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_add_xx_0, 0), (self.gr_sub_xx_0, 1))
		self.connect((self.gr_add_xx_0, 0), (self.probe_vsrc, 0))
		self.connect((self.gr_sub_xx_0, 0), (self.gr_vco_f_0, 0))
		self.connect((self.gr_vco_f_0, 0), (self.gr_hilbert_fc_0, 0))
		self.connect((self.gr_hilbert_fc_0, 0), (self.uhd_usrp_sink_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_sub_xx_0, 0))
		self.connect((self.gr_threshold_ff_0, 0), (self.gr_add_xx_0, 0))
		self.connect((self.gr_threshold_ff_0_0, 0), (self.gr_add_xx_0, 1))
		self.connect((self.gr_threshold_ff_0_1, 0), (self.gr_add_xx_0, 2))
		self.connect((self.gr_threshold_ff_0_2, 0), (self.gr_add_xx_0, 3))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_threshold_ff_0_2, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_threshold_ff_0_1, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_threshold_ff_0_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_threshold_ff_0, 0))
		self.connect((self.const_source_x_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.uhd_usrp_source_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_multiply_xx_0_0, 0))
		self.connect((self.gr_hilbert_fc_0, 0), (self.wxgui_scopesink2_1, 1))
		self.connect((self.gr_hilbert_fc_0, 0), (self.gr_multiply_xx_0_0, 1))
		self.connect((self.gr_throttle_0, 0), (self.wxgui_scopesink2_1, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_throttle_0, 0))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.low_pass_filter_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.gr_throttle_0_0, 0))
		self.connect((self.gr_throttle_0_0, 0), (self.wxgui_fftsink2_0, 0))

	def get_probe_var(self):
		return self.probe_var

	def set_probe_var(self, probe_var):
		self.probe_var = probe_var
		self.set_probed_value(self.probe_var)
		self.uhd_usrp_source_0.set_center_freq(500000000+self.probe_var*100000000, 0)
		self.uhd_usrp_sink_0.set_center_freq(500000000+self.probe_var*100000000, 0)

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)
		self.wxgui_scopesink2_1.set_sample_rate(self.samp_rate)
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 10000, 1000, firdes.WIN_HAMMING, 6.76))
		self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)
		self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)

	def get_probed_value(self):
		return self.probed_value

	def set_probed_value(self, probed_value):
		self.probed_value = probed_value
		self._probed_value_static_text.set_value(self.probed_value)

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = top_block()
	tb.Run(True)

